'use strict';
var BaseView = require('base-view');

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

var DiscussionView = require('views/discussion-view');

module.exports = BaseView.extend({
    template: templates['discussion'],

    el: '#content',

    // initialize: function (argument) {
    //     this.BaseView.prototype.initialize.apply(this, arguments);


    // }

    discussionViews: [],

    render: function () {
        var discussionView;
        _.each(this.collection.models, function (model) {
            discussionView = new DiscussionView({
                model: model
            });
            this.$el.append(discussionView.render().$el);
            discussionView.prepEditor();
            this.discussionViews.push(discussionView);
        }, this);
    }
});

var BaseCollection = Backbone.Collection;

var DiscussionModel = require('models/discussion-model');

module.exports = BaseCollection.extend({

    model: DiscussionModel,

    url: '/web/discussion.json',

    parse: function (json) {
        return json.topics;
    }
})
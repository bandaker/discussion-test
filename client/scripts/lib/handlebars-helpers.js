'use strict';

var Handlebars = require('handlebars');
var templates = require('templates')(Handlebars);

Handlebars.registerHelper({

    timeAgo: function (age) {
        var dateNow = new Date();
        return moment(new Date(dateNow.getTime() - age)).fromNow()
    },

    textStr: function (text) {
        return new Handlebars.SafeString(text);
    }

});

module.exports = Handlebars;

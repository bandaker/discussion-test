'use strict';

var DiscussionCollection = require('collections/discussion-collection');
var TopicsView = require('views/topics-view');

require('lib/handlebars-helpers');

function App() {
}

App.prototype = _.extend(App.prototype, {
    initialize: function () {
        var discussionCollection = new DiscussionCollection();
        discussionCollection.fetch({
            success: function () {
                var view = new TopicsView({
                    collection: discussionCollection
                });

                view.render();
            }
        });
    }
});

module.exports = App;
